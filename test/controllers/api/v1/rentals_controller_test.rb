require 'test_helper'
require_relative '../../../../app/controllers/api/v1/rentals_controller'

module Api
  module V1
    class RentalsControllerTest < ActionDispatch::IntegrationTest
      setup do
        @token = 'Token token="secret"'

        post('/api/v1/rentals',
             params: {
               daily_rate: 42.2,
               name: 'Test'
             },
             headers: { 'AUTHORIZATION' => @token })

        @data = @response.body
        @id = JSON.parse(@response.body)['data']['id']
      end

      test 'should get all rentals' do
        get('/api/v1/rentals', headers: { 'AUTHORIZATION' => @token })
        assert_response :success
        assert_match "\"id\":\"#{@id}\"", @response.body
      end

      test 'should create rental' do
        assert_difference('Rental.count', 1) do
          post('/api/v1/rentals',
               params: {
                 daily_rate: 42.2,
                 name: 'Three'
               },
               headers: { 'AUTHORIZATION' => @token })
        end
      end

      test 'should show a rental' do
        get("/api/v1/rentals/#{@id}", headers: { 'AUTHORIZATION' => @token })

        assert_response :success
        assert_equal @data, @response.body
      end

      test 'should not show non-existing rental' do
        get('/api/v1/rentals/fake_id', headers: { 'AUTHORIZATION' => @token })

        assert_response :not_found
      end

      test 'should update a rental' do
        put("/api/v1/rentals/#{@id}",
            params: { 'name' => 'fooboo' },
            headers: { 'AUTHORIZATION' => @token })

        new_name = JSON.parse(@response.body)['data']['attributes']['name']

        assert_response :success
        assert_equal 'fooboo', new_name
      end

      test 'should destroy rental' do
        assert_difference('Rental.count', -1) do
          delete("/api/v1/rentals/#{@id}",
                 headers: { 'AUTHORIZATION' => @token })
        end
      end
    end
  end
end
