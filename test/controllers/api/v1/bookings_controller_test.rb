require 'test_helper'
require_relative '../../../../app/controllers/api/v1/bookings_controller'

module Api
  module V1
    class BookingsControllerTest < ActionDispatch::IntegrationTest
      setup do
        @token = 'Token token="secret"'

        post('/api/v1/rentals',
             params: {
               daily_rate: 42.2,
               name: 'Test'
             },
             headers: { 'AUTHORIZATION' => @token })

        @rental_id = JSON.parse(@response.body)['data']['id']

        post('/api/v1/bookings',
             params: {
               start_at: '2017-08-19 20:20:56',
               end_at: '2017-08-20 20:20:56',
               client_email: 'benjamin.vanryseghem@gmail.com',
               rental_id: @rental_id
             },
             headers: { 'AUTHORIZATION' => @token })

        @data = @response.body
        @id = JSON.parse(@response.body)['data']['id']
      end

      test 'should get all bookings' do
        get('/api/v1/bookings', headers: { 'AUTHORIZATION' => @token })

        assert_response :success
        assert_match "\"id\":\"#{@id}\"", @response.body
      end

      test 'should get all bookings of rental' do
        get("/api/v1/rentals/#{@rental_id}/bookings",
            headers: { 'AUTHORIZATION' => @token })

        assert_response :success
        assert_match "\"id\":\"#{@id}\"", @response.body
      end

      test 'should create booking' do
        assert_difference('Booking.count', 1) do
          post('/api/v1/bookings',
               params: {
                 start_at: '2017-08-22 20:20:56',
                 end_at: '2017-08-23 20:20:56',
                 client_email: 'benjamin.vanryseghem@gmail.com',
                 rental_id: @rental_id
               },
               headers: { 'AUTHORIZATION' => @token })
        end
      end

      test 'should create booking belonging to a rental' do
        assert_difference('Booking.count', 1) do
          post("/api/v1/rentals/#{@rental_id}/bookings",
               params: {
                 start_at: '2017-08-22 20:20:56',
                 end_at: '2017-08-23 20:20:56',
                 client_email: 'benjamin.vanryseghem@gmail.com'
               },
               headers: { 'AUTHORIZATION' => @token })
        end
      end

      test 'should show a booking' do
        get("/api/v1/bookings/#{@id}", headers: { 'AUTHORIZATION' => @token })

        assert_response :success
        assert_equal @data, @response.body
      end

      test 'should show a booking belonging to a rental' do
        get("/api/v1/rentals/#{@rental_id}/bookings/#{@id}",
            headers: { 'AUTHORIZATION' => @token })

        assert_response :success
        assert_equal @data, @response.body
      end

      test 'should not show a non-existing booking' do
        get('/api/v1/bookings/fake_id', headers: { 'AUTHORIZATION' => @token })

        assert_response :not_found
      end

      test 'should not show a booking not belonging to a rental' do
        post('/api/v1/rentals',
             params: {
               daily_rate: 42.2,
               name: 'Test'
             },
             headers: { 'AUTHORIZATION' => @token })

        rental_id = JSON.parse(@response.body)['data']['id']

        post('/api/v1/bookings',
             params: {
               start_at: '2017-08-20 20:20:56',
               end_at: '2017-08-21 20:20:56',
               client_email: 'benjamin.vanryseghem@gmail.com',
               rental_id: rental_id
             },
             headers: { 'AUTHORIZATION' => @token })

        id = JSON.parse(@response.body)['data']['id']

        get("/api/v1/rentals/#{@rental_id}/bookings/#{id}",
            headers: { 'AUTHORIZATION' => @token })

        assert_response :not_found
      end

      test 'should update a booking' do
        put("/api/v1/bookings/#{@id}",
            params: { 'client_email' => 'foo@boo.com' },
            headers: { 'AUTHORIZATION' => @token })

        new_email = JSON.parse(@response.body).fetch('data')
                        .fetch('attributes')
                        .fetch('client-email')

        assert_response :success
        assert_equal 'foo@boo.com', new_email
      end

      test 'should update a booking belonging to a rental' do
        put("/api/v1/rentals/#{@rental_id}/bookings/#{@id}",
            params: { 'client_email' => 'foo@boo.com' },
            headers: { 'AUTHORIZATION' => @token })

        new_email = JSON.parse(@response.body).fetch('data')
                        .fetch('attributes')
                        .fetch('client-email')

        assert_response :success
        assert_equal 'foo@boo.com', new_email
      end

      test 'should destroy booking' do
        assert_difference('Booking.count', -1) do
          delete("/api/v1/bookings/#{@id}",
                 headers: { 'AUTHORIZATION' => @token })
        end
      end

      test 'should destroy booking belonging to a rental' do
        assert_difference('Booking.count', -1) do
          delete("/api/v1/rentals/#{@rental_id}/bookings/#{@id}",
                 headers: { 'AUTHORIZATION' => @token })
        end
      end
    end
  end
end
