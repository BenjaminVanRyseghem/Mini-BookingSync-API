require 'test_helper'
require 'date'

class BookingTest < ActiveSupport::TestCase
  test 'should validates fixtures' do
    bookings(:one, :two).each do |booking|
      assert booking.save
    end
  end

  test 'should not have bookings without client_email' do
    booking = Booking.new(
      start_at: '2017-09-04T16:28:26+02:00'.to_datetime,
      end_at: '2017-09-05T16:28:26+02:00'.to_datetime,
      rental: rentals(:one)
    )

    assert_not booking.save
  end

  test 'should not have bookings with too short period' do
    booking = Booking.new(
      start_at: '2017-09-04T16:28:26+02:00'.to_datetime,
      end_at: '2017-09-04T16:28:26+02:00'.to_datetime,
      client_email: 'benjamin.vanryseghem @gmail.com',
      rental: rentals(:one)
    )

    assert_not booking.save
  end

  test 'should not have bookings with overlapping periods' do
    booking = Booking.new(
      start_at: '2017-08-20 10:20:56'.to_datetime,
      end_at: '2017-08-21 10:20:56'.to_datetime,
      client_email: 'benjamin.vanryseghem@gmail.com',
      rental: rentals(:one)
    )

    assert_not booking.save
  end

  test 'should not have bookings with non email client_email' do
    booking = Booking.new(
      start_at: '2017-09-04T16:28:26+02:00'.to_datetime,
      end_at: '2017-09-05T16:28:26+02:00'.to_datetime,
      rental: rentals(:one),
      client_email: 43
    )

    assert_not booking.save
  end

  test 'should save after updating price' do
    booking = bookings(:one)
    update = booking.stubs(:update_price)
    save = booking.stubs(:save!)
    booking.update_price!

    assert update.once
    assert save.once
  end

  test 'should ask rental for computing price' do
    booking = bookings(:one)
    rental = booking.rental
    price = rental.stubs(:price_for_booking)
    booking.send(:update_price)

    assert price.with(booking)
  end
end
