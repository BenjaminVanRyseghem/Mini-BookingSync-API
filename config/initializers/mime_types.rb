# Be sure to restart your server when you modify this file.

# Add new mime types for use in respond_to blocks:
# Mime::Type.register "text/richtext", :rtf

# Mime::Type.register "application/json", :json, %w( text/x-json application/jsonrequest application/vnd.api+json )

mime_type = Mime::Type.lookup('application/vnd.api+json').symbol
ActionDispatch::Request.parameter_parsers[mime_type] = lambda do |body|
  ActiveModelSerializers::Deserialization.jsonapi_parse(JSON.parse(body))
end
