Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api do
    namespace :v1 do
      resources :rentals do
        resources :bookings
      end

      resources :bookings
    end
  end
end
