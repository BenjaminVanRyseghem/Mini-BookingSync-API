# A Rental is a place that can be rented.
#  It has 0 to many bookings (with no overlap)
class Rental < ApplicationRecord
  has_many :bookings, dependent: :destroy
  validates :daily_rate, :name, presence: true

  before_save :update_daily_rate

  def price_for_booking(booking)
    price_for_range(booking.start_at, booking.end_at)
  end

  def date_overlapping?(start_at, end_at, booking = nil)
    to_check = booking ? bookings.where.not(id: booking.id) : bookings
    overlaps = to_check.where(
      '(start_at BETWEEN :start_at AND :end_at) OR
       (end_at BETWEEN :start_at AND :end_at) OR
       (:start_at BETWEEN start_at AND end_at) OR
       (:end_at BETWEEN start_at AND end_at)',
      start_at: start_at,
      end_at: end_at
    )

    !overlaps.empty?
  end

  protected

  def price_for_range(start_at, end_at)
    (end_at.to_date - start_at.to_date) * daily_rate
  end

  private

  def update_daily_rate
    bookings.each(&:update_price!)
  end
end
