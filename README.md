# Mini Booking Sync API

[![pipeline status](https://gitlab.com/BenjaminVanRyseghem/Mini-BookingSync-API/badges/master/pipeline.svg)](https://gitlab.com/BenjaminVanRyseghem/Mini-BookingSync-API/commits/master)
[![coverage report](https://gitlab.com/BenjaminVanRyseghem/Mini-BookingSync-API/badges/master/coverage.svg)](https://gitlab.com/BenjaminVanRyseghem/Mini-BookingSync-API/commits/master)

Exercise made in the process of hiring. Assignment can be found [here](https://www.bookingsync.com/en/jobs/2017-04-03-senior-full-stack-rails-ember-js-developer).

## How to

### Initialize

```
	gem install bundler
	bundle install
	rails db:migrate
	rails db:seed
```

### Tasks

To start the server, run

```
	rails server
```

To run tests, run

```
	rails test
```
