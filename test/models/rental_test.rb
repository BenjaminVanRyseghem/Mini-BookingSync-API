require 'test_helper'

class RentalTest < ActiveSupport::TestCase
  test 'should call price_for_range when computing price of a booking' do
    rental = rentals(:one)
    booking = bookings(:one)
    price = rental.stubs(:price_for_range)
    rental.price_for_booking(booking)

    assert price.once
  end

  test 'should compute correct price for ranges' do
    rental = rentals(:one)
    data = [
      ['2017-08-19 20:20:56', '2017-08-19 20:20:56', 0],
      ['2017-08-19 20:20:56', '2017-08-20 20:20:56', 42.2],
      ['2017-08-19 20:20:56', '2017-08-19 20:21:56', 0],
      ['2017-08-19 23:59:59', '2017-08-20 00:00:01', 42.2]
    ]

    data.each do |start_at, end_at, result|
      start_date = start_at.to_datetime
      end_date = end_at.to_datetime
      assert_equal result, rental.send(:price_for_range, start_date, end_date)
    end
  end

  test 'should compute overlapping bookings' do
    rental = rentals(:one)
    data = [
      ['2017-08-19 20:20:56', '2017-08-19 20:20:56', true],
      ['2017-08-20 20:20:57', '2017-08-21 20:20:56', false],
      ['2017-08-18 20:20:56', '2017-08-19 20:20:55', false],
      ['2017-08-18 20:20:56', '2017-08-21 20:20:56', true],
      ['2017-08-19 20:21:56', '2017-08-19 20:22:56', true]
    ]

    data.each do |start_at, end_at, result|
      start_date = start_at.to_datetime.utc
      end_date = end_at.to_datetime.utc

      assert_equal result, rental.date_overlapping?(start_date, end_date)
    end
  end

  test 'should not considered the passed booking' do
    rental = rentals(:one)
    booking = rental.bookings[0]

    assert_not rental.date_overlapping?(booking.start_at,
                                        booking.end_at,
                                        booking)
  end

  test 'should update all bookings price' do
    rental = rentals(:one)
    stubs = rental.bookings.map do |booking|
      booking.stubs(:update_price!)
    end
    rental.send(:update_daily_rate)

    stubs.each do |stub|
      assert stub.once
    end
  end
end
