# Validate attribute as an email address
class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return if value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
    record.errors[attribute] << (options[:message] || 'is not an email')
  end
end

# Validate attribute as a booking range, and ensure there are no overlapping
# bookings
class DateRangeValidator < ActiveModel::Validator
  def long_enough?(start_at, end_at)
    (end_at - start_at) >= 1
  end

  def validate(record)
    if record.rental.date_overlapping?(record.start_at, record.end_at, record)
      record.errors[:base] << 'Dates are overlapping'
    end

    return if long_enough?(record.start_at, record.end_at)
    record.errors[:base] << 'Booking should at least be one day long'
  end
end

# Booking for a given Rental.
# A booking is a range of time during which a rental is booked for a client.
class Booking < ApplicationRecord
  belongs_to :rental

  validates_with DateRangeValidator
  validates :start_at, :end_at, :price, presence: true
  validates :client_email, presence: true, email: true

  before_validation :update_price

  def update_price!
    update_price
    save!
  end

  protected

  def update_price
    self.price = rental.price_for_booking(self)
  end
end
