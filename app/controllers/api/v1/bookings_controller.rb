module Api
  module V1
    # Controller for managing bookings
    #
    # It implements a CRUD through REST
    # The data sent follow the JSONAPI (http://jsonapi.org) format
    #
    # Routes are available as top level, or as a sub-path of rentals
    class BookingsController < V1Controller
      before_action :fetch_bookings
      before_action :fetch_booking, only: %i[show update destroy]
      before_action :fetch_params, only: %i[create update]

      def index
        render json: @bookings
      end

      def create
        rental = Rental.find(params[:rental_id])
        @booking = rental.bookings.create @params
        @booking.save!
        render json: @booking
      end

      def show
        render json: @booking
      end

      def update
        @booking.update! @params
        render json: @booking
      end

      def destroy
        @booking.destroy!
        render json: { errors: [] }
      end

      private

      def fetch_bookings
        @bookings = if params[:rental_id].present?
                      Rental.find(params[:rental_id]).bookings
                    else
                      Booking.all
                    end
      end

      def fetch_booking
        @booking = @bookings.find(params[:id])
      end

      def fetch_params
        @params = params.permit(
          :start_at,
          :end_at,
          :client_email
        )
      end
    end
  end
end
