
module Api
  module V1
    # Controller for managing rentals
    #
    # It implements a CRUD through REST
    # The data sent follow the JSONAPI (http://jsonapi.org) format
    class RentalsController < V1Controller
      before_action :fetch_rentals
      before_action :fetch_rental, only: %i[show update destroy]
      before_action :fetch_params, only: %i[create update]

      def index
        render json: @rentals
      end

      def create
        @rental = Rental.new(@params)
        @rental.save!
        render json: @rental
      end

      def show
        render json: @rental
      end

      def update
        @rental.update! @params
        render json: @rental
      end

      def destroy
        @rental.destroy!
        render json: { errors: [] }
      end

      private

      def fetch_rentals
        @rentals = Rental.all
      end

      def fetch_rental
        @rental = @rentals.find(params[:id])
      end

      def fetch_params
        @params = params.permit(
          :daily_rate,
          :name
        )
      end
    end
  end
end
