module Api
  module V1
    # Superclass for all the V1 controllers
    # Encapsulate the authentication, and the error recovering
    class V1Controller < ApplicationController
      include ActionController::HttpAuthentication::Basic::ControllerMethods
      include ActionController::HttpAuthentication::Token::ControllerMethods

      # This should be loaded from an Env Var, but the assignment advise
      # to keep it simple
      TOKEN = 'secret'.freeze

      before_action :authenticate
      rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

      def record_not_found
        render json: { errors: ['Record not found'] },
               status: 404
      end

      def authenticate
        authenticate_or_request_with_http_token do |token, _options|
          ActiveSupport::SecurityUtils.secure_compare(
            ::Digest::SHA256.hexdigest(token),
            ::Digest::SHA256.hexdigest(TOKEN)
          )
        end
      end
    end
  end
end
