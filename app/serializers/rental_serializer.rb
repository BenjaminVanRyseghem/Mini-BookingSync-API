# Rental serializer including related bookings
class RentalSerializer < ActiveModel::Serializer
  attributes :id, :daily_rate, :name
  has_many :bookings, embed: :ids, key: 'bookings', include: true
end
